window.MathJax = {
    TeX: {
        Macros: {
            vv: ["{\\vec{#1}}", 1],
            vi: "{\\vec{i}}",
            vj: "{\\vec{j}}",
            hvecT: ["{(#1,#2)^{\\mathstrut\\scriptscriptstyle{T}}}",2]
        }
    }
}

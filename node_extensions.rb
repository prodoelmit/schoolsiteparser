module Sexp 


    class Document < Treetop::Runtime::SyntaxNode
        def cl
            return "document"
        end
        def to_html
            s = %&<!DOCTYPE html>
            <html>
            <head>
            <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
            <script type="text/javascript" src="jquery-2.2.1.js">
            </script>
            <script type="text/javascript" src="main.js">
            </script>
            <link rel="stylesheet" href="style.css"/>
            <script src="mathjax_settings.js" type="text/javascript"></script>
            <script src='https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML'> </script>
            </head>
            <body>&

            elements.each do |e|
#                binding.pry
                unless e.nil?
                    if (e.is_a? Content)
                        s += e.paragraph_to_html
                    else 
                        s += e.to_html
                    end
                end
            end
            s += "</body></html>"

            return s
        end
    end

    class ContentBlock < Treetop::Runtime::SyntaxNode
        def cl 
            return "ContBlock"
        end
        def to_html
            #s = " CBLOCKSTART"
            s = ""
            elements.each do |e|
                a = e.to_html
                s += a unless a.nil?
            end
            #s += " CBLOCKEND"
            return s
        end
    end

    class Content < Treetop::Runtime::SyntaxNode
        def cl
            return "Content"
        end
        def to_html
            #s = " CONTENTBEGIN"
            s = ""
            elements.each do |e|
                unless e.nil?
                    s += " <br>#{e.to_html}"
                end
            end
            #s += " CONTENTEND"
            return s
        end

        def paragraph_to_html
            #binding.pry
            s = "<p>#{self.to_html}</p>"
            return s

        end


    end

    class Environment < Treetop::Runtime::SyntaxNode
        def cl
            return "environment"
        end
        def to_html
            if (elements[1].text_value == "enumerate") 
                return Enumerate::to_html       
            else 
                s =  %$<div class="$
                s += elements[1].text_value
                s += %$">$
                s += elements[3].to_html
                s += %$</div>$
                return s
            end
        end


    end

    class Itemize < Treetop::Runtime::SyntaxNode
        def cl
            return "itemize"
        end
        def to_html
            s = "<ul>"
            items.each do |i|
                s += i.to_html
            end
            s += "</ul>"
            return s
        end
        
        def items
            return elements[2].elements
        end
    end

    class ListItem < Treetop::Runtime::SyntaxNode
        def cl
            return "listitem"
        end
        def to_html
            return %$<li>#{elements[1]}</li>$
        end
    end
    class Enumerate < Treetop::Runtime::SyntaxNode
        def cl
            return "enumerate"
        end

        def to_html
            s = "<ol>"
            items.each do |i|
                s += i.to_html
            end
            s += "</ul>"
            return s
        end
        
        def items
            return elements[2].elements
        end
    end


    class Command < Treetop::Runtime::SyntaxNode
        def cl
            return "command"
        end
        def to_html
            case name
            when "section"
                return section_to_html
            when "subsection"
                return subsection_to_html
            when "firstdef"
                return firstdef_to_html
            when "textit"
                return textit_to_html
            when "primech"
                return primech_to_html
            when "cImg"
                return cImg_to_html
            when "ref"
                return ref_to_html
            when "primer"
                return primer_to_html
            when "label"
                TexConverter::RefIndex.registerLabel(args[0], TexConverter::Parser.currentPage)
                return %-<span class="label" id="#{args[0]}"></span>-
            when "geogebra"
                return %-<iframe scrolling="no" src="https://www.geogebra.org/material/iframe/id/#{args[0]}/width/1000/height/400/border/888888/rc/false/ai/false/sdz/true/smb/false/stb/false/stbh/true/ld/false/sri/true/at/auto" width="1000px" height="400px" style="border:0px;"> </iframe>-
            when "'"
                return %-#{args[0]}\u0301-
            when "linktosection"
                return %-<a class="linktosection" href="<%= TexConverter::RefIndex.getPageByLabel("#{args[0]}") %>">#{args[1]}</a>-
            else 
                if (name == "section") 
                    return section_to_html
                end
                s = "<span>"
                s += elements[2].text_value
                args.each do |a|
                    s += " #{a} :::: #{self.name}"
                end
                s += "</span>"
                return s
            end
        end

        def args
            return elements[3].elements.map {|e| e.elements[1].text_value}
        end
        
        def argsR
            return elements[3].elements.map {|e| e.elements[1]} 
        end

        def name
            return elements[2].text_value
        end

        def value
            return elements[3].text_value
        end

        def section_to_html
            s = "</p><h1>#{args[0]}</h1><p>"
            return s
        end

        def subsection_to_html
            s = "</p><h2>#{args[0]}</h2><p>"
            return s
        end

        def firstdef_to_html
            s = %-<span class="firstdef">#{args[0]}</span>-
            return s
        end

        def textit_to_html
            s = %-<i>#{args[0]}</i>-
            return s
        end

        def primech_to_html
            s = "<b>Примечание:</b>"
            return s
        end

        def cImg_to_html
            counter = TexConverter::Counter.getCounter("img")
            s = %&<div class="cImg" counter="#{counter}"><span class="label" id="#{args[3]}"></span><img cwidth="#{args[0]}" src="figures/#{args[1]}.png" /><div class="caption">Рисунок #{counter} \&mdash; #{argsR[2].to_html}</div></div>&

        end

        def ref_to_html

            s = %&<a class="ref" href="##{args[0]}" ref="#{args[0]}">?</a>&
        end

        def primer_to_html
            s = %&<span class="primer">Пример: </span>&

        end
        

    end
    

    class Text < Treetop::Runtime::SyntaxNode
        def cl
            return "text"
        end
        def to_html
            #s = " TEXTBEGIN#{text_value}TEXTEND"
            s = text_value.gsub("\\\\","<br>")
            s = s.gsub("---","&mdash;")
            s = s.gsub("``","&ldquo;")
            s = s.gsub("''","&rdquo;")
            return s
        end

    end
    
    class LineBreaks < Treetop::Runtime::SyntaxNode
        def cl
            return "linebreaks"
        end
        def to_html
            return "</p> <p>"
        end

    end

    class Word < Treetop::Runtime::SyntaxNode
        def cl 
            return "word"
        end
        def to_html
            return text_value
        end
    end

    class LineBreak < Treetop::Runtime::SyntaxNode
        def cl
            return "linebreak"
        end
        def to_html
            #binding.pry
            #return "BR"
        end
    end

    class SpaceLiteral < Treetop::Runtime::SyntaxNode
        
        def cl
            return "spaceliteral"
        end
        def to_html
            #return "SP"
        end
    end

    class TextLiteral < Treetop::Runtime::SyntaxNode
        def to_html
            return text_value
        end
    end

    class InlineFormula < Treetop::Runtime::SyntaxNode
        def cl
            return "inlform"
        end
        def to_html
            content = elements[1].text_value
            content = content.gsub(/\\vv/,"\\vec")

            s = '\('
            s += elements[1].text_value
            s += '\)'
            return s
        end

    end


    class DollarFormula < Treetop::Runtime::SyntaxNode
        def cl
            return "dolform"
        end
        def to_html
            #puts "compiling equation: \n #{elements[1]}"
            s = %-<div class="equationstar">-
            s += %-<img src="#{TexConverter::TexCompiler.compile_equation(elements[1].text_value)}"/>-
            s += %-</div>-
            return s
        end

    end


    class BracketFormula < Treetop::Runtime::SyntaxNode
        def cl
            return "bracform"
        end
        def to_html
            s = %-<div class="equationstar">-
            s += %-<img src="#{TexConverter::TexCompiler.compile_equation(elements[1].text_value)}"/>-
            s += %-</div>-
            return s
        end

    end

    class Equation < Treetop::Runtime::SyntaxNode
        def cl
            return "eq"
        end
        def to_html
            counter = TexConverter::Counter.getCounter("equation")
            re = Regexp.new "\\label{(?<label>[a-zA-Z0-9:]+)}"
            match = text_value.match re
            unless (match.nil?) 
                label = match[1]
            end


            
            s = %-<div class="equation" counter="#{counter}">-
            s += %-<span class="label" id="#{label}"></span>- unless label.nil?
            s += %-<div class="eqcounter">(#{counter})</div><img src="#{TexConverter::TexCompiler.compile_equation(elements[1].text_value)}"/></div>-
            return s
        end

    end

    class EquationStar < Treetop::Runtime::SyntaxNode
        def cl
            return "eqstar"
        end
        def to_html
            s = %-<div class="equationstar"><img src="#{TexConverter::TexCompiler.compile_equation(elements[1].text_value)}"/></div>-
            return s
        end

    end

    class Align < Treetop::Runtime::SyntaxNode
        def cl
            return "align"
        end
        def to_html
            counter = TexConverter::Counter.getCounter("equation")
            re = Regexp.new "\\label{(?<label>[a-zA-Z0-9:]+)}"
            match = text_value.match re
            unless (match.nil?) 
                label = match[1]
            end

            s = %-<div class="align" counter="#{counter}">-
            s += %-<span class="label" id="#{label}"></span>- unless label.nil?
            s += %-<div class="eqcounter">(#{counter})</div><img src="#{TexConverter::TexCompiler.compile_align(elements[1].text_value)}"/></div>-


            return s
        end

    end

    class AlignStar < Treetop::Runtime::SyntaxNode
        def cl
            return "alignstar"
        end
        def to_html
            s = %-<div class="alignstar"><img src="#{TexConverter::TexCompiler.compile_align(elements[1].text_value)}"/></div>-

            return s
        end

    end

    class FormulaText < Treetop::Runtime::SyntaxNode

    end
    class FormulaEnvironment < Treetop::Runtime::SyntaxNode

    end

    
    class CommandArgContent < Treetop::Runtime::SyntaxNode


    end


end

class Label
    def initialize(name)
        @@name = name
    end

    def to_html
        %-<span id="#{@@name}"></span>-
    end


end

module Treetop
    module Runtime
        class SyntaxNode
            def inspect 
                return text_value
            end
            def to_html
                if ((not nil?) and
                    (not elements.nil?) and
                    elements.length > 0)
                    s = ""
                    elements.each do |e|
                        unless e.nil?
                            a = e.to_html
                            s += a unless a.nil?
                        end
                    end
                    return s
                else 
                    return ""
                end
            end
        end
    end
end

require 'securerandom'
module TexConverter 
    class TexCompiler
        def self.setPreamble filename
            @@preamble = File.read(filename)
        end

        def self.setTmpFolder folderPath
            `mkdir #{folderPath} -p`
            @@tmpFolder = folderPath
        end

        def self.setOutFolder folderPath
            `mkdir #{folderPath} -p`
            @@outFolder = folderPath
        end

        def self.setDisplayOutFolder folderPath
            @@displayOutFolder = folderPath
        end

        def self.compile_equation(eq)
            TexHandler.compileLogWrite %-Compiling equation:\n#{eq}-
            
            #binding.pry
            s = @@preamble
            s += "\n\\begin{document}\n\\[\n#{eq}\n\\]\n\\end{document}"

            return compile(s)
        end

        def self.compile_align(eq)
            TexHandler.compileLogWrite %-Compiling align:\n#{eq}-
            s = @@preamble
            s += "\n\\begin{document}\n\\begin{align*}\n#{eq}\n\\end{align*}\n\\end{document}"

            return compile(s)
        end


        def self.compile(content)
            tex_filename = "#{@@tmpFolder}/#{random_string}"
            pngRandomString = random_string
            png_filename = "#{@@outFolder}/#{pngRandomString}"
            png_display_filename = "#{@@displayOutFolder}/#{pngRandomString}"
            tex_file = File.new("#{tex_filename}.tex", "w")
            tex_file = File.new("#{tex_filename}.tex", "w")
            tex_file.write(content)
            tex_file.close
            cmd = "openout_any=a pdflatex -jobname=#{png_filename} -interaction=nonstopmode #{tex_filename}.tex"
            compile_result = %x( #{cmd} )
            cmd2 = "pdftoppm -r 180 #{png_filename}.pdf | pnmcrop | pnmtopng > #{png_filename}.png"
            crop_result = %x( #{cmd2} )
            path = "#{png_display_filename}.png"
            return path
        end

        def self.random_string
            string ||= SecureRandom.urlsafe_base64
        end

    end
end

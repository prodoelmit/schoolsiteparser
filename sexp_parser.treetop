grammar Sexp
    rule document
         (content linebreaks? linebreak?)* <Document>
    end

    rule content
        (contentblock)+ <Content>
    end
    
    rule contentblock
        (equationstar / alignstar / equation /  align / enumerate  / itemize  / environment  / command  / dollarformula  / inlineformula  / bracketformula  / text  /  space / comment)+  <ContentBlock>
    end

    rule comment
        '%' text &(linebreak)
    end

    rule equation
        '\\begin{equation}' formulacontent  '\\end{equation}' <Equation>
    end

    rule equationstar
        '\\begin{equation*}' formulacontent  '\\end{equation*}' <EquationStar>
    end

    rule align
        '\\begin{align}' formulacontent  '\\end{align}' <Align>
    end

    rule alignstar
        '\\begin{align*}' formulacontent  '\\end{align*}' <AlignStar>
    end
    
    rule enumerate
        '\\begin{enumerate}' space* linebreak (space* listitem linebreak)+ '\\end{enumerate}' <Enumerate>
    end

    rule itemize
        '\\begin{itemize}' space* linebreak  (space* listitem linebreak)+   '\\end{itemize}' <Itemize>
    end

    rule environment
        '\\begin{' !('equation'/'itemize'/'enumerate'/'align') [a-zA-Z0-9\*]+ '}' (space* content? space*) '\\end{' [a-zA-Z0-9\*]+ &{|seq| seq[1].text_value == seq[5].text_value  } '}' <Environment>
    end



    rule listitem
        '\\item ' content  <ListItem>
    end

    rule command
        '\\' !('begin' / 'end' / 'item') [a-zA-Z\']+ ('{' commandargcontent '}')* &{|seq| seq[1].text_value != 'begin' and seq[1].text_value != 'end'} <Command>

    end

    rule linebreaks
        linebreak 2.. <LineBreaks>
    end

    rule dollarformula
        '$$' formulacontent '$$' <DollarFormula>
    end

    rule bracketformula
        '\\[' formulacontent '\\]' <BracketFormula>
    end

    rule inlineformula
        '$'  formulacontent   '$' <InlineFormula>
    end

    rule text
        (space)* (word (space)* )+ <Text> 
    end

    rule word
        (textliteral)+ <Word>
    end

    rule formulacontent
        (command / formulaenvironment / formulatext / '\\ ' / '\\,' )+ 
    end
    
    rule formulaenvironment
        '\\begin{' [a-zA-Z0-9\*]+ '}' (space* formulacontent? space*) '\\end{' [a-zA-Z0-9\*]+ &{|seq| seq[1].text_value == seq[5].text_value and seq[1].text_value != "equation" and seq[1].text_value != "equation*" } '}' <FormulaEnvironment>
    end

    rule formulatext
        formulaliteral+ <FormulaText>
    end

    rule textliteral
        ( [^\s\%\$#\_\{\}~\\\^] 1..1 <TextLiteral> /  ("\\" [\%\$\#\_\{\}~\\\^] )   <TextLiteral> ) 
    end

    rule commandargcontent
        (inlineformula / text / '_')+  <CommandArgContent>
    end


    rule formulaliteral
        ([^\\$] 1..1  / "\\$"  / "\\\\" )
    end

    rule commandliteral
        [a-zA-Z] 
    end

    rule linebreak
        "\r"? "\n" <LineBreak>
    end

    rule space 
        [ \t]+ <SpaceLiteral>
    end
    





end
